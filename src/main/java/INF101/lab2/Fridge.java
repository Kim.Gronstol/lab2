package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private final List<FridgeItem> items;
    private final int MAX_SIZE;

    public Fridge() {
        this.items = new ArrayList<>();
        this.MAX_SIZE = 20;
    }

    @Override
    public int nItemsInFridge() {return items.size();}

    @Override
    public int totalSize() {return MAX_SIZE;}

    @Override
    public boolean placeIn(FridgeItem item) {
        if(items.size() < MAX_SIZE) {
            items.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)) {
            items.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {items.clear();}

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expired.add(item);
            }
        }
        for (FridgeItem item : expired) {
            items.remove(item);
        }
        return expired;
    }
}
